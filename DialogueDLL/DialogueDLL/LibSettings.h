#pragma once
#define DIALOGUE_EXPORT


#ifdef DIALOGUE_EXPORT
#define LIB_API __declspec(dllexport)
#elif DIALOGUE_EXPORT
#define LIB_API __declspec(dllimport)
#else
#define LIB_API
#endif