﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour {

	public Dialogue dialogue;
    
    public void TriggerDialogue () {

        //can use singleton instead
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
    }
}
