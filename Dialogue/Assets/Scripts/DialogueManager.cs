﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using System;

public class DialogueManager : MonoBehaviour {

    public Text nameText;
    public Text dialogueText;

    //public Animator animator;

    private Queue<string> sentences;

    [DllImport("Dialogue")]
    public static extern IntPtr GetLines (string fileName);

    [DllImport("Dialogue")]
    public static extern IntPtr HelloWorld();

	// Use this for initialization
	void Start () {

        string input = Marshal.PtrToStringAnsi(GetLines("D:/Dialogue/Assets/Plugins/testDialogue.txt"));
        Debug.Log(Marshal.PtrToStringAnsi(GetLines("D:/Dialogue/Assets/Plugins/testDialogue.txt")));
        Debug.Log (input);
        //Debug.Log (Marshal.PtrToStringAnsi(HelloWorld()));
        sentences = new Queue<string>();

	}

    public void StartDialogue (Dialogue dialogue) {

        //animator.SetBool("IsOpen", true);

        //Debug.Log ("Starting conversation with " + dialogue.name);
        nameText.text = dialogue.name;

        sentences.Clear();

        string input = Marshal.PtrToStringAnsi(GetLines("D:/Dialogue/Assets/Plugins/testDialogue.txt"));
        string[] temp = input.Split('.');


        foreach (string sentence in temp) {
            
            sentences.Enqueue(sentence + ".");
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence () {

        if (sentences.Count == 0) {

            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        dialogueText.text = sentence;
        //StopAllCoroutines ();
        //StartCoroutine(TypeSetence(sentence));
        //Debug.Log(sentence);
    }

    IEnumerator TypeSentence (string sentence) {

        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray()) {

            dialogueText.text += letter;
            yield return null;
        }
    }

    void EndDialogue () {

        //animator.SetBool("IsOpen", false);
        Debug.Log ("End of conversation");
    }
}
